import React from 'react'

function NavBar() {
  return (
    <div className='flex justify-between p-5 items-center border-b-[1px] shadow-sm' >
        <h2 className='font-light tracking-widest'>Car Rental</h2>
        <div className='flex gap-5'>
            <h2 className='hover:bg-blue-500 hover:text-white p-2 rounded-full px-3 cursor-pointer'>Home</h2>
            <h2 className='hover:bg-blue-500 hidden sm:block hover:text-white p-2 rounded-full px-3 cursor-pointer'>History</h2>
            <h2 className='hover:bg-blue-500 hidden sm:block hover:text-white p-2 rounded-full px-3 cursor-pointer'>Contact Us</h2>
        </div>
        <div>
            <h2>User</h2>
        </div>
    </div>
  )
}

export default NavBar