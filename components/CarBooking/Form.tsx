import React from 'react'

function Form() {
  return (
    <div>

<div className='flex flex-col w-full mb-5'>
            <label className='text-gray-400'>PickUp Location</label>
            <input type="text" placeholder="Type here" 
            className="input input-bordered w-full max-w-lg" />
        </div>
    <div className='flex flec-col gap-5 mb-5'>
       
        <div className='flex flex-col w-full'>
            <label className='text-gray-400'>Pick Up Date</label>
            <input type="date" placeholder="Type here" 
            className="input input-bordered w-full max-w-lg" />
        </div>
        <div className='flex flex-col w-full'>
            <label className='text-gray-400'>Drop Off Date</label>
            <input type="date" placeholder="Type here" 
            className="input input-bordered w-full max-w-lg" />
        </div>
       
    </div>
    <div className='flex gap-5 '>
    <div className='flex flex-col w-full mb-5'>
            <label className='text-gray-400'>Pick Up Time</label>
            <input type="time" placeholder="Type here" 
            className="input input-bordered w-full max-w-lg" />
        </div>
        <div className='flex flex-col w-full mb-5'>
            <label className='text-gray-400'>Drop Off Time</label>
            <input type="time" placeholder="Type here" 
            className="input input-bordered w-full max-w-lg" />
        </div>
    </div>
   
    <div className='flex flex-col w-full mb-5'>
            <label className='text-gray-400'>Contact Number</label>
            <input type="text" placeholder="Type here" 
            className="input input-bordered w-full max-w-lg" />
        </div>
    </div>
  )
}

export default Form