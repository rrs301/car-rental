import React from 'react'

function CarsFiltersOption() {
    return (
        <div className='mt-10 flex justify-between'>
            <div>
                <h2 className='text-[30px] font-bold'>Cars Catalog</h2>
                <h2>Explore our cars you might likes</h2>

            </div>
            <div className='sm:flex gap-3 hidden '>
            <select className="select select-bordered w-[100px] max-w-xs">
                <option disabled selected>Price</option>
                <option>Han Solo</option>
                <option>Greedo</option>
            </select>
            <select className="select select-bordered w-[200px] max-w-xs">
                <option disabled selected>Manufactural</option>
                <option>Han Solo</option>
                <option>Greedo</option>
            </select>
            </div>
        </div>
    )
}

export default CarsFiltersOption