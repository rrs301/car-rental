import Image from 'next/image'
import React, { useState } from 'react'
import { FaGasPump } from "react-icons/fa";

import { MdAirlineSeatReclineNormal } from "react-icons/md";
import { PiSteeringWheelFill } from "react-icons/pi";
import CarBookingModal from '../CarBooking/CarBookingModal';
import CarCard from './CarCard';
function CarsList(props: any) {
    const [selectedCar,setSelectedCar]=useState<any>([]);
    return (
        <div className='grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 mt-10'>
            {props.carsList.map((car: any, index: number) => (
                <div  onClick={()=>{(window as any).my_modal_1.showModal();setSelectedCar(car)}}>
            <CarCard car={car} 
              />
                </div>
             
            ))}

            <dialog id="my_modal_1" className="modal">
            <CarBookingModal car={selectedCar} />
            </dialog>
        </div>
    )
}

export default CarsList