import React from 'react'

function SearchInput() {
    return (
        <div>
            <h2 className='text-center mb-3 text-[20px] text-gray-400'>Lets Search what you need</h2>
        <div className='flex justify-center '>
           
            <div className='bg-gray-100 p-2 rounded-full flex gap-3 px-6'>
            <div className='flex items-center gap-2'>
              
                <input type='text' placeholder='Location'
                className='p-2 outline-none bg-transparent ' />
                  <svg xmlns="http://www.w3.org/2000/svg" 
                viewBox="0 0 24 24" fill="currentColor" 
                className="w-4 h-4 text-gray-800">
                    <path fillRule="evenodd" d="M11.54 22.351l.07.04.028.016a.76.76 0 00.723 0l.028-.015.071-.041a16.975 16.975 0 001.144-.742 19.58 19.58 0 002.683-2.282c1.944-1.99 3.963-4.98 3.963-8.827a8.25 8.25 0 00-16.5 0c0 3.846 2.02 6.837 3.963 8.827a19.58 19.58 0 002.682 2.282 16.975 16.975 0 001.145.742zM12 13.5a3 3 0 100-6 3 3 0 000 6z" clipRule="evenodd" />
                </svg>
            </div>
            <div className='flex items-center gap-2'>
           

                <input type='date' placeholder='Select PickUp Date'
                className='p-2 outline-none bg-transparent ' />
            </div>
           
            </div>
        </div>
        </div>
    )
}

export default SearchInput