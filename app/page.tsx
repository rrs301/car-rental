"use client"
import CarsFiltersOption from '@/components/Home/CarsFiltersOption'
import CarsList from '@/components/Home/CarsList'
import Hero from '@/components/Home/Hero'
import SearchInput from '@/components/Home/SearchInput'
import { getCarsList } from '@/services'
import Image from 'next/image'
import { useEffect, useState } from 'react'

export default function Home() {

  const [carsList,setCarsList]=useState<any>([]);
  useEffect(()=>{
    getCarsListData();
  },[])
  const getCarsListData=async()=>{
    const resp:any=await getCarsList();
    console.log(resp);
    setCarsList(resp?.carsLists)
  }
  return (
   <div className='p-5 sm:px-10 md:px-20'>
    <Hero/>
    <SearchInput/>
    <CarsFiltersOption  />
    <CarsList carsList={carsList}/>
   </div>
  )
}
