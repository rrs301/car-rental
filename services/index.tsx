import request, { gql } from "graphql-request"

export const getCarsList=async()=>{
const query = gql`
query MyQuery {
    carsLists {
      image {
        url
        id
      }
      name
      price
      seats
      type
      carAvg
    }
  }  
`
    const result=await request('https://api-us-east-1-shared-usea1-02.hygraph.com/v2/clhknn8y243je01uoedbof8un/master', query)
    return result;
}